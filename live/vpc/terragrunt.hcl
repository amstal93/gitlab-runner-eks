terraform {
  source = "git::git@gitlab.com:polarsquad/gitlab/gitlab-runner-eks.git//modules/vpc?ref=v1.0.4"
}


include {
  path = find_in_parent_folders()
}

locals {
  common-vars  = yamldecode(file("${find_in_parent_folders("common-vars.yaml")}"))
}

inputs = {
  aws_region  = local.common-vars.aws_region
  profile     = local.common-vars.profile
  environment = local.common-vars.environment
  cidr = "10.10.0.0/16"
  azs = ["eu-central-1a","eu-central-1b","eu-central-1c"]
  public_subnets = ["10.10.0.0/20", "10.10.16.0/20", "10.10.32.0/20"]
  private_subnets = ["10.10.48.0/20","10.10.112.0/20", "10.10.128.0/20"]

}
